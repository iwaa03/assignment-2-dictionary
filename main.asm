%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define BUFFER_SIZE 256
%define NEW_LINE 10
%define ERROR_CODE 1

section .rodata

too_long: db "your key is too long", 0xA, 0
not_found: db "There is no such key in the dictionary", 0xA, 0

section .bss

buffer: RESB 256

section .text

global _start

_start:

    xor rcx, rcx
.read_word:
    push rcx
    call read_char
    pop rcx
    cmp al, NEW_LINE
    je .end
    mov [rcx+buffer], al
    inc rcx
    cmp rcx, BUFFER_SIZE
    jne .read_word
    mov rdi, too_long 
    call print_string
    mov rdi, ERROR_CODE
    call exit
.finish_program:
	call print_string
	mov rdi, 0
	call exit

.not_found:
	mov rdi, not_found
	jmp .finish_program

.end:
    mov byte[rsp + rcx], 0
    mov rsi, begining
    mov rdi, buffer
    call find_word
    test rax, rax
    jz .not_found
    mov rdi, rax
    add rdi, 8
    call string_length
    lea rdi, [rdi + rax + 1]
    jmp .finish_program

