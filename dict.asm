%include "lib.inc"
%define BYTE 8

section .text

global find_word
find_word:
    mov r11, rdi
    .loop:
        add rsi, BYTE
        call string_equals
        sub rsi, BYTE
        test rax, rax 
        jnz .success
        mov rsi, [rsi]
        test rsi, rsi
        jnz .loop
    .success:
        mov rax, rsi
	mov rdi, r11
        ret
